#include "StdAfx.h"
#include "IsInputTextEmpty.h"
#include "UIFuryInterface.h"

CIsInputTextEmpty::CIsInputTextEmpty(string sNodeName, IUIFuryNode * pNewParent, bool empty, SInterfaceObject *pRefInterfaceObj, IUIFuryInterface *pNewInterface)
	: IUIFuryNode(sNodeName, pNewParent, pRefInterfaceObj, pNewInterface)
	, bEmpty(empty)
{
}

void CIsInputTextEmpty::RunNode()
{
	if (!pRefObj || !pInterface)
		return;

	if (pRefObj->sLabelText.empty())
	{
		if (bEmpty)
		{
			IUIFuryNode::RunNode();
		}
	}
	else
	{
		if (!bEmpty)
		{
			IUIFuryNode::RunNode();
		}
	}
}

