#include "StdAfx.h"
#include "ShowNode.h"
#include "UIFuryInterface.h"

CShowNode::CShowNode(string sNodeName, IUIFuryNode * pNewParent, bool show, SInterfaceObject *pRefInterfaceObj, IUIFuryInterface *pNewInterface)
	: IUIFuryNode(sNodeName, pNewParent, pRefInterfaceObj, pNewInterface)
	, bShow(show)
{
}

void CShowNode::Run()
{
	if (!pRefObj || !pInterface)
		return;

	pInterface->ShowControl(pRefObj->sName, bShow);
}
