/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : UIFury Component
Purpose : Interface component can be either single entity or can be added to any other entity

--------------------------------------------------------------------------------- */

#pragma once

#include <CryEntitySystem/IEntityComponent.h>
#include "UIFuryInterface.h"
#include <CrySchematyc/Utils/SharedString.h>
#include "FuryResources.h"

struct IUIFuryNode;

struct SUIFuryInterfaceSettings
{
	inline bool operator==(const SUIFuryInterfaceSettings& rhs) const { return 0 == memcmp(this, &rhs, sizeof(rhs)); }
	inline bool operator!=(const SUIFuryInterfaceSettings& rhs) const { return 0 != memcmp(this, &rhs, sizeof(rhs)); }

	//Editor members
	UIFuryFile uiFuryFile;
	bool bShowByDefault;
};
static void ReflectType(Schematyc::CTypeDesc<SUIFuryInterfaceSettings>& desc)
{
	desc.SetGUID("{8FBCB897-70F7-470F-B18C-0F867DA89C5D}"_cry_guid);
	desc.SetLabel("UIFury Interface Settings");
	desc.SetDescription("");
	desc.AddMember(&SUIFuryInterfaceSettings::uiFuryFile, 'uifi', "UIFuryFile", "UI Fury file", "", "");
	desc.AddMember(&SUIFuryInterfaceSettings::bShowByDefault, 'show', "ShowByDefault", "Show interface by default", "", false);
}

class CUIFuryInterfaceComponent : public IEntityComponent
{
public:
	CUIFuryInterfaceComponent() = default;
	CUIFuryInterfaceComponent(string sUIElementName, bool bShow);
	CUIFuryInterfaceComponent::~CUIFuryInterfaceComponent() {}
	// IEntityComponent
	virtual void Initialize() override;
	virtual uint64 GetEventMask() const override;
	virtual void ProcessEvent(SEntityEvent& event) override;
	static void ReflectType(Schematyc::CTypeDesc<CUIFuryInterfaceComponent>& desc);
	virtual void Update(float frameTime);
	// ~IEntityComponent

	IUIFuryInterface *GetInterface() { return pInterface; }
	
	//SCHEMATYC FUNCTIONS
	void Schematyc_ShowControl(Schematyc::CSharedString controlName);
	void Schematyc_HideControl(Schematyc::CSharedString controlName);
	void Schematyc_SetLabelText(Schematyc::CSharedString labelName, Schematyc::CSharedString labelText);
	void Schematyc_AddItemToDropDownList(Schematyc::CSharedString listName, Schematyc::CSharedString itemValue);
	void Schematyc_ResetDropDownList(Schematyc::CSharedString listName, bool bClear);
	void Schematyc_AddItemToSearchList(Schematyc::CSharedString listName, Schematyc::CSharedString itemValue, bool bExpandable);
	void Schematyc_AddDropItemToTarget(Schematyc::CSharedString itemName, int uniqueId, Schematyc::CSharedString targetName, Schematyc::CSharedString dropClass, Schematyc::CSharedString dropClass_second, int width, int height, Schematyc::CSharedString img, bool snap);
	void Schematyc_AddDropTarget(Schematyc::CSharedString parentControl, Schematyc::CSharedString name, int x, int y, int width, int height, Schematyc::CSharedString imgBase, Schematyc::CSharedString imgAvailable, Schematyc::CSharedString imgNotAvailable, Schematyc::CSharedString dropClass);
	void Schematyc_ClearDropTarget(Schematyc::CSharedString dropTargetName);
	//~SCHEMATYC FUNCTIONS
	//SCHEMATYC SINGAL TRIGGERS
	void Schematyc_OnButtonPressedEvent(Schematyc::CSharedString buttonName);
	void Schematyc_OnButtonReleasedEvent(Schematyc::CSharedString buttonName);
	void Schematyc_OnObjectMouseEnterEvent(Schematyc::CSharedString objectName);
	void Schematyc_OnObjectMouseLeaveEvent(Schematyc::CSharedString objectName);
	void Schematyc_OnSwitchButtonEvent(Schematyc::CSharedString objectName);
	void Schematyc_OnInputTextValueChangedEvent(Schematyc::CSharedString inputTextName, Schematyc::CSharedString currentValue);
	void Schematyc_OnDropDownListValueChangedEvent(Schematyc::CSharedString dropDownListName, Schematyc::CSharedString currentValue);
	void Schematyc_OnSearchListValueChangedEvent(Schematyc::CSharedString searchListName, Schematyc::CSharedString currentValue);
	void Schematyc_OnDropItemEvent(int uniqueId, Schematyc::CSharedString dropTargetName);
	//~SCHEMATYC SIGNAL TRIGGERS

	void Open();
	void Close();
	bool Toggle();
	virtual bool IsOpened() { return bIsOpened; }

	void CallbackEvent_Set_SearchList(void(*callback)(string, string));
	void CallbackEvent_Set_DropDownList(void(*callback)(string, string));
	void CallbackEvent_Set_ButtonReleased(void(*callback)(string, string));
	void CallbackEvent_Set_ButtonPressed(void(*callback)(string, string));
	void CallbackEvent_Set_SwitchButton(void(*callback)(string, bool));
	void CallbackEvent_Set_InputText(void(*callback)(string, string));
	void CallbackEvent_Set_ControlEnterLeave(void(*callback)(string, bool));
	void CallbackEvent_Set_DropItemEnterLeave(void(*callback)(int, bool));

private:
	void ComponentPropertyChanged();
	void LoadInterface();
	void LoadInterfaceHierarchy(XmlNodeRef parentXmlNode, string parentName);
	void LoadSearchListItem(XmlNodeRef parentXmlNode, string listName);
	void LoadInterfaceLogic(XmlNodeRef parentXmlNode, IUIFuryNode *pParentNode);

private:
	IUIFuryInterface *pInterface = nullptr;
	SUIFuryInterfaceSettings settings;
	bool bIsOpened = false;
	bool bIsRuntime = false;

public:
	const string uiFolderPath = "Libs/UI/";
	const string uiElementsPath = uiFolderPath + "UIElements/";
	const string uiActionsPath = uiFolderPath + "UIActions/";
	const string gfx = ".gfx";
	const string show = "show";
	const string hide = "hide";
};
