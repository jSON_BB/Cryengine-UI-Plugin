#pragma once

struct SInterfaceObject
{
	SInterfaceObject(int listIdx, string objName, int x, int y, int w, int h, string labelText, string labelBaseColor, string labelHoverColor, string labelPressColor, string labelFont, int fontSize, string baseImage, string hoverImage, string pressImage, string additionalImage, string dropClass);
	~SInterfaceObject() {}

	//all properties are public
	//constants/unchangable
	const string sName;
	const string sLabelBaseColor;
	const string sLabelHoverColor;
	const string sLabelPressColor;
	const string sLabelFont;
	const string sImageBase;
	const string sImageHover;
	const string sImagePress;
	const string sImageAdditional;
	const string sDropClass;
	const int iWidth;
	const int iHeight;
	const int iFontSize;
	const int iListIndex;
	//changable
	string sLabelText;
	int iXPos;
	int iYPos;
};