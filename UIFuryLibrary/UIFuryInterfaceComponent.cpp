#include "StdAfx.h"
#include "UIFuryInterfaceComponent.h"
#include <CrySchematyc/Env/Elements/EnvComponent.h>
#include <CrySchematyc/Env/Elements/EnvFunction.h>
#include <CrySchematyc/Env/IEnvRegistrar.h>
#include <CrySchematyc/Env/Elements/EnvFunction.h>
#include <CrySchematyc/Env/Elements/EnvSignal.h>
#include <CrySchematyc/IObject.h>
#include "EventNode.h"
#include "ShowNode.h"
#include "IsInputTextEmpty.h"
#include "CVarNode.h"

struct SButtonPressedEventSignal { Schematyc::CSharedString objectName; };
struct SButtonReleasedEventSignal { Schematyc::CSharedString objectName; };
struct SObjectMouseEnterEventSignal { Schematyc::CSharedString objectName; };
struct SObjectMouseLeaveEventSignal { Schematyc::CSharedString objectName; };
struct SSwitchButtonEventSignal { Schematyc::CSharedString objectName; };
struct SInputTextValueChangedEventSignal { Schematyc::CSharedString objectName; Schematyc::CSharedString currentValue; };
struct SDropDownListValueChangedEventSignal { Schematyc::CSharedString objectName; Schematyc::CSharedString currentValue; };
struct SSeachListValueChangedEventSignal { Schematyc::CSharedString objectName; Schematyc::CSharedString currentValue; };
struct SDropItemEventSignal { int uniqueId; Schematyc::CSharedString dropTargetName; };

#pragma region SCHEMATYC_REGISTRATION

static void RegisterCEntityComponent(Schematyc::IEnvRegistrar& registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CUIFuryInterfaceComponent));
		//Functions
		{
			auto pFunction = SCHEMATYC_MAKE_ENV_FUNCTION(&CUIFuryInterfaceComponent::Schematyc_ShowControl, "{8BA50331-797E-4C35-A461-778DA715FCED}"_cry_guid, "ShowObject");
			pFunction->SetDescription("Shows specified object");
			pFunction->BindInput(1, 'obnm', "ObjectName");
			componentScope.Register(pFunction);
		}
		{
			auto pFunction = SCHEMATYC_MAKE_ENV_FUNCTION(&CUIFuryInterfaceComponent::Schematyc_HideControl, "{8D8473B2-EE65-492E-BA57-225E1195B65C}"_cry_guid, "HideObject");
			pFunction->SetDescription("Hides specified object");
			pFunction->BindInput(1, 'obnm', "ObjectName");
			componentScope.Register(pFunction);
		}
		{
			auto pFunction = SCHEMATYC_MAKE_ENV_FUNCTION(&CUIFuryInterfaceComponent::Schematyc_SetLabelText, "{ACEF5F3F-5E9B-41F7-8874-B37704690C66}"_cry_guid, "SetLabelText");
			pFunction->SetDescription("Sets new text for specified label");
			pFunction->BindInput(1, 'labn', "LabelName");
			pFunction->BindInput(2, 'labt', "LabelText");
			componentScope.Register(pFunction);
		}
		{
			auto pFunction = SCHEMATYC_MAKE_ENV_FUNCTION(&CUIFuryInterfaceComponent::Schematyc_AddItemToDropDownList, "{683D6E33-0675-4E1C-9C23-54829ED57E60}"_cry_guid, "AddItemToDropDownList");
			pFunction->SetDescription("Adds item to specified drop down list");
			pFunction->BindInput(1, 'lina', "ListName");
			pFunction->BindInput(2, 'ival', "ItemName");
			componentScope.Register(pFunction);
		}
		{
			auto pFunction = SCHEMATYC_MAKE_ENV_FUNCTION(&CUIFuryInterfaceComponent::Schematyc_ResetDropDownList, "{CB277DB7-BE1A-493C-95F4-585A5D655250}"_cry_guid, "ResetDropDownList");
			pFunction->SetDescription("Resets drop down list");
			pFunction->BindInput(1, 'lina', "ListName");
			pFunction->BindInput(2, 'clea', "Clear", "Specifies whether we want list to be cleared out completely");
			componentScope.Register(pFunction);
		}
		//Singnals
		{
			auto pSignal = SCHEMATYC_MAKE_ENV_SIGNAL(SButtonPressedEventSignal);
			pSignal->SetDescription("Triggered when button is pressed");
			componentScope.Register(pSignal);
		}
		{
			auto pSignal = SCHEMATYC_MAKE_ENV_SIGNAL(SButtonReleasedEventSignal);
			pSignal->SetDescription("Triggered when button is released");
			componentScope.Register(pSignal);
		}
		{
			auto pSignal = SCHEMATYC_MAKE_ENV_SIGNAL(SObjectMouseEnterEventSignal);
			pSignal->SetDescription("Triggered when mouse cursor enters object");
			componentScope.Register(pSignal);
		}
		{
			auto pSignal = SCHEMATYC_MAKE_ENV_SIGNAL(SObjectMouseLeaveEventSignal);
			pSignal->SetDescription("Triggered when cursor leaves object");
			componentScope.Register(pSignal);
		}
		{
			auto pSignal = SCHEMATYC_MAKE_ENV_SIGNAL(SInputTextValueChangedEventSignal);
			pSignal->SetDescription("Triggered when input text value changes");
			componentScope.Register(pSignal);
		}
		{
			auto pSignal = SCHEMATYC_MAKE_ENV_SIGNAL(SDropDownListValueChangedEventSignal);
			pSignal->SetDescription("Triggered when drop down list value changes");
			componentScope.Register(pSignal);
		}
		{
			auto pSignal = SCHEMATYC_MAKE_ENV_SIGNAL(SSeachListValueChangedEventSignal);
			pSignal->SetDescription("Triggered when search list value is selected");
			componentScope.Register(pSignal);
		}
		{
			auto pSignal = SCHEMATYC_MAKE_ENV_SIGNAL(SSwitchButtonEventSignal);
			pSignal->SetDescription("Triggered when switch is activated");
			componentScope.Register(pSignal);
		}
		{
			auto pSignal = SCHEMATYC_MAKE_ENV_SIGNAL(SDropItemEventSignal);
			pSignal->SetDescription("Triggered item is dropped on drop target");
			componentScope.Register(pSignal);
		}
	}
}

static void ReflectType(Schematyc::CTypeDesc<SButtonPressedEventSignal>&desc)
{
	desc.SetGUID("{64B7D674-6EFA-4856-992D-5BCF73CEAB1B}"_cry_guid);
	desc.SetLabel("On Button Pressed");
	desc.AddMember(&SButtonPressedEventSignal::objectName, 'obnm', "ObjectName", "Button name", "Internal name of the button", Schematyc::CSharedString());
}
static void ReflectType(Schematyc::CTypeDesc<SButtonReleasedEventSignal>&desc)
{
	desc.SetGUID("{0F3FA4BF-3125-4968-9882-39AB6A84AB6A}"_cry_guid);
	desc.SetLabel("On Button Released");
	desc.AddMember(&SButtonReleasedEventSignal::objectName, 'obnm', "ObjectName", "Button name", "Internal name of the button", Schematyc::CSharedString());
}
static void ReflectType(Schematyc::CTypeDesc<SObjectMouseEnterEventSignal>&desc)
{
	desc.SetGUID("{C266373B-04DF-45E8-9125-A3C0132F712D}"_cry_guid);
	desc.SetLabel("On Object Mouse Enter");
	desc.AddMember(&SObjectMouseEnterEventSignal::objectName, 'obnm', "ObjectName", "Object name", "Internal name of the object", Schematyc::CSharedString());
}
static void ReflectType(Schematyc::CTypeDesc<SObjectMouseLeaveEventSignal>&desc)
{
	desc.SetGUID("{1342FD91-EE27-4DBC-923C-3779B2AEE3BD}"_cry_guid);
	desc.SetLabel("On Object Mouse Leave");
	desc.AddMember(&SObjectMouseLeaveEventSignal::objectName, 'obnm', "ObjectName", "Object name", "Internal name of the object", Schematyc::CSharedString());
}
static void ReflectType(Schematyc::CTypeDesc<SInputTextValueChangedEventSignal>&desc)
{
	desc.SetGUID("{85903BA3-4B33-4284-BF38-6645BD0C0AF7}"_cry_guid);
	desc.SetLabel("On Input Text Value Changed");
	desc.AddMember(&SInputTextValueChangedEventSignal::objectName, 'obnm', "ObjectName", "Object name", "Internal name of the input text", Schematyc::CSharedString());
	desc.AddMember(&SInputTextValueChangedEventSignal::currentValue, 'curv', "CurrentValue", "Current value", "Current value of input text", Schematyc::CSharedString());
}
static void ReflectType(Schematyc::CTypeDesc<SDropDownListValueChangedEventSignal>&desc)
{
	desc.SetGUID("{FC93E994-D6E3-414E-ACF4-5BD3F954D90A}"_cry_guid);
	desc.SetLabel("On Drop Down List Value Changed");
	desc.AddMember(&SDropDownListValueChangedEventSignal::objectName, 'obnm', "ObjectName", "Object name", "Internal name of the drop down list", Schematyc::CSharedString());
	desc.AddMember(&SDropDownListValueChangedEventSignal::currentValue, 'curv', "CurrentValue", "Current value", "Current value of drop down list", Schematyc::CSharedString());
}
static void ReflectType(Schematyc::CTypeDesc<SSeachListValueChangedEventSignal>&desc)
{
	desc.SetGUID("{FCE6E4E5-582C-49DA-8789-FE6ED6E2D8FC}"_cry_guid);
	desc.SetLabel("On Search List Value Selected");
	desc.AddMember(&SSeachListValueChangedEventSignal::objectName, 'obnm', "ObjectName", "Object name", "Internal name of the search list", Schematyc::CSharedString());
	desc.AddMember(&SSeachListValueChangedEventSignal::currentValue, 'curv', "CurrentValue", "Current value", "Current value of search list", Schematyc::CSharedString());
}
static void ReflectType(Schematyc::CTypeDesc<SSwitchButtonEventSignal>&desc)
{
	desc.SetGUID("{81CB9119-9BBC-40B8-9966-6B9D6F95A3F8}"_cry_guid);
	desc.SetLabel("On Switch Button Event");
	desc.AddMember(&SSwitchButtonEventSignal::objectName, 'obnm', "ObjectName", "Object name", "Internal name of the switch button", Schematyc::CSharedString());
}
static void ReflectType(Schematyc::CTypeDesc<SDropItemEventSignal>&desc)
{
	desc.SetGUID("{53FA47EF-8930-4B3A-B477-D8573821E987}"_cry_guid);
	desc.SetLabel("On Drop Item Event");
	desc.AddMember(&SDropItemEventSignal::uniqueId, 'uniq', "UniqueId", "Unique id", "Internal unique id number of the drop item", 0);
	desc.AddMember(&SDropItemEventSignal::dropTargetName, 'tarn', "DropTargetName", "Drop target name", "Internal name of drop target", Schematyc::CSharedString());
}

CRY_STATIC_AUTO_REGISTER_FUNCTION(&RegisterCEntityComponent)

#pragma endregion SCHEMATYC_REGISTRATION

CUIFuryInterfaceComponent::CUIFuryInterfaceComponent(string sUIElementName, bool bShow)
	: bIsRuntime(true)
{
	settings.uiFuryFile.value = "libs/UI/" + sUIElementName + ".fury";
	settings.bShowByDefault = bShow;
}

void CUIFuryInterfaceComponent::Initialize()
{
	if (!gEnv->IsClient() || gEnv->IsEditor())
		return;

	if (!pInterface)
	{
		pInterface = new IUIFuryInterface(this);
		if(bIsRuntime)
			ComponentPropertyChanged();
	}
}

uint64 CUIFuryInterfaceComponent::GetEventMask() const
{
	return ENTITY_EVENT_BIT(ENTITY_EVENT_UPDATE) | ENTITY_EVENT_BIT(ENTITY_EVENT_COMPONENT_PROPERTY_CHANGED) | ENTITY_EVENT_BIT(ENTITY_EVENT_START_GAME);
}

void CUIFuryInterfaceComponent::ProcessEvent(SEntityEvent & event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_UPDATE:
	{
		SEntityUpdateContext* pCtx = (SEntityUpdateContext*)event.nParam[0];
		Update(pCtx->fFrameTime);
	}
	break;
	case ENTITY_EVENT_COMPONENT_PROPERTY_CHANGED:
	{
		//ComponentPropertyChanged();
	}
	break;
	case ENTITY_EVENT_START_GAME:
	{
		if(!bIsRuntime)
			ComponentPropertyChanged();
	}
	break;
	}
}

void CUIFuryInterfaceComponent::ReflectType(Schematyc::CTypeDesc<CUIFuryInterfaceComponent>& desc)
{
	desc.SetGUID("{C797DAC7-7704-443E-A0E5-6EB28BECD4E7}"_cry_guid);
	desc.SetEditorCategory("UIFury");
	desc.SetLabel("UIFury Interface Component");
	desc.SetDescription("Can be either single entity or be attached to any other entity");
	desc.SetComponentFlags({ IEntityComponent::EFlags::Transform, IEntityComponent::EFlags::Socket, IEntityComponent::EFlags::Attach });
	//
	desc.AddMember(&CUIFuryInterfaceComponent::settings, 'sett', "Settings", "UIFury Settings", "", SUIFuryInterfaceSettings());
}

void CUIFuryInterfaceComponent::Update(float frameTime)
{
	if (!gEnv->IsClient() || !pInterface)
		return;
}

#pragma region SCHEMATYC
#pragma region FUNCTIONS
void CUIFuryInterfaceComponent::Schematyc_ShowControl(Schematyc::CSharedString controlName)
{
	if (!pInterface || controlName.empty())
		return;

	pInterface->ShowControl(controlName.c_str(), true);
}

void CUIFuryInterfaceComponent::Schematyc_HideControl(Schematyc::CSharedString controlName)
{
	if (!pInterface || controlName.empty())
		return;

	pInterface->ShowControl(controlName.c_str(), false);
}
void CUIFuryInterfaceComponent::Schematyc_SetLabelText(Schematyc::CSharedString labelName, Schematyc::CSharedString labelText)
{
	if (!pInterface || labelName.empty())
		return;

	pInterface->SetLabelText(labelName.c_str(), labelText.c_str());
}
void CUIFuryInterfaceComponent::Schematyc_AddItemToDropDownList(Schematyc::CSharedString listName, Schematyc::CSharedString itemValue)
{
	if (!pInterface || listName.empty() || itemValue.empty())
		return;

	pInterface->AddItemToDropDownList(listName.c_str(), itemValue.c_str());
}
void CUIFuryInterfaceComponent::Schematyc_ResetDropDownList(Schematyc::CSharedString listName, bool bClear)
{
	if (!pInterface || listName.empty())
		return;

	pInterface->ResetDropDownList(listName.c_str(), bClear);
}
void CUIFuryInterfaceComponent::Schematyc_AddItemToSearchList(Schematyc::CSharedString listName, Schematyc::CSharedString itemValue, bool bExpandable)
{
	if (!pInterface || listName.empty() || itemValue.empty())
		return;

	pInterface->AddItemToSearchList(listName.c_str(), itemValue.c_str(), bExpandable);
	pInterface->AddSearchListEventCaller(listName.c_str());
}
void CUIFuryInterfaceComponent::Schematyc_AddDropItemToTarget(Schematyc::CSharedString itemName, int uniqueId, Schematyc::CSharedString targetName, Schematyc::CSharedString dropClass, Schematyc::CSharedString dropClass_second, int width, int height, Schematyc::CSharedString img, bool snap)
{
	if (!pInterface || itemName.empty() || targetName.empty() || dropClass.empty())
		return;

	pInterface->AddDropItemToTarget(itemName.c_str(), uniqueId, targetName.c_str(), dropClass.c_str(), dropClass_second.c_str(), width, height, img.c_str(), snap);
}
void CUIFuryInterfaceComponent::Schematyc_AddDropTarget(Schematyc::CSharedString parentControl, Schematyc::CSharedString name, int x, int y, int width, int height, Schematyc::CSharedString imgBase, Schematyc::CSharedString imgAvailable, Schematyc::CSharedString imgNotAvailable, Schematyc::CSharedString dropClass)
{
	if (!pInterface || parentControl.empty() || name.empty())
		return;

	pInterface->AddDropTarget(parentControl.c_str(), name.c_str(), x, y, width, height, imgBase.c_str(), imgAvailable.c_str(), imgNotAvailable.c_str(), dropClass.c_str());
}
void CUIFuryInterfaceComponent::Schematyc_ClearDropTarget(Schematyc::CSharedString dropTargetName)
{
	if (!pInterface || dropTargetName.empty())
		return;

	pInterface->ClearDropTarget(dropTargetName.c_str());
}
#pragma endregion FUNCTIONS
#pragma region SIGNALS
void CUIFuryInterfaceComponent::Schematyc_OnButtonPressedEvent(Schematyc::CSharedString buttonName)
{
	if (buttonName.empty())
		return;

	if (Schematyc::IObject *pSchematycObject = m_pEntity->GetSchematycObject())
	{
		pSchematycObject->ProcessSignal(SButtonPressedEventSignal{ Schematyc::CSharedString(buttonName) }, GetGUID());
	}
}

void CUIFuryInterfaceComponent::Schematyc_OnButtonReleasedEvent(Schematyc::CSharedString buttonName)
{
	if (buttonName.empty())
		return;

	if (Schematyc::IObject *pSchematycObject = m_pEntity->GetSchematycObject())
	{
		pSchematycObject->ProcessSignal(SButtonReleasedEventSignal{ Schematyc::CSharedString(buttonName) }, GetGUID());
	}
}

void CUIFuryInterfaceComponent::Schematyc_OnObjectMouseEnterEvent(Schematyc::CSharedString objectName)
{
	if (objectName.empty())
		return;

	if (Schematyc::IObject *pSchematycObject = m_pEntity->GetSchematycObject())
	{
		pSchematycObject->ProcessSignal(SObjectMouseEnterEventSignal{ Schematyc::CSharedString(objectName) }, GetGUID());
	}
}

void CUIFuryInterfaceComponent::Schematyc_OnObjectMouseLeaveEvent(Schematyc::CSharedString objectName)
{
	if (objectName.empty())
		return;

	if (Schematyc::IObject *pSchematycObject = m_pEntity->GetSchematycObject())
	{
		pSchematycObject->ProcessSignal(SObjectMouseLeaveEventSignal{ Schematyc::CSharedString(objectName) }, GetGUID());
	}
}

void CUIFuryInterfaceComponent::Schematyc_OnSwitchButtonEvent(Schematyc::CSharedString objectName)
{
	if (objectName.empty())
		return;

	if (Schematyc::IObject *pSchematycObject = m_pEntity->GetSchematycObject())
	{
		pSchematycObject->ProcessSignal(SSwitchButtonEventSignal{ Schematyc::CSharedString(objectName) }, GetGUID());
	}
}

void CUIFuryInterfaceComponent::Schematyc_OnInputTextValueChangedEvent(Schematyc::CSharedString inputTextName, Schematyc::CSharedString currentValue)
{
	if (inputTextName.empty())
		return;

	if (Schematyc::IObject *pSchematycObject = m_pEntity->GetSchematycObject())
	{
		pSchematycObject->ProcessSignal(SInputTextValueChangedEventSignal{ Schematyc::CSharedString(inputTextName), Schematyc::CSharedString(currentValue) }, GetGUID());
	}
}

void CUIFuryInterfaceComponent::Schematyc_OnDropDownListValueChangedEvent(Schematyc::CSharedString dropDownListName, Schematyc::CSharedString currentValue)
{
	if (dropDownListName.empty())
		return;

	if (Schematyc::IObject *pSchematycObject = m_pEntity->GetSchematycObject())
	{
		pSchematycObject->ProcessSignal(SDropDownListValueChangedEventSignal{ Schematyc::CSharedString(dropDownListName), Schematyc::CSharedString(currentValue) }, GetGUID());
	}
}

void CUIFuryInterfaceComponent::Schematyc_OnSearchListValueChangedEvent(Schematyc::CSharedString searchListName, Schematyc::CSharedString currentValue)
{
	if (searchListName.empty())
		return;

	if (Schematyc::IObject *pSchematycObject = m_pEntity->GetSchematycObject())
	{
		pSchematycObject->ProcessSignal(SSeachListValueChangedEventSignal{ Schematyc::CSharedString(searchListName), Schematyc::CSharedString(currentValue) }, GetGUID());
	}
}
void CUIFuryInterfaceComponent::Schematyc_OnDropItemEvent(int uniqueId, Schematyc::CSharedString dropTargetName)
{
	if (dropTargetName.empty())
		return;

	if (Schematyc::IObject *pSchematycObject = m_pEntity->GetSchematycObject())
	{
		pSchematycObject->ProcessSignal(SDropItemEventSignal{ uniqueId, dropTargetName }, GetGUID());
	}
}
void CUIFuryInterfaceComponent::Open()
{
	if (!pInterface)
		return;

	pInterface->ShowElement();
	bIsOpened = true;
}
void CUIFuryInterfaceComponent::Close()
{
	if (!pInterface)
		return;

	pInterface->HideElement();
	bIsOpened = false;
}
bool CUIFuryInterfaceComponent::Toggle()
{
	bIsOpened = !bIsOpened;

	bIsOpened ? Open() : Close();

	return bIsOpened;
}
void CUIFuryInterfaceComponent::CallbackEvent_Set_SearchList(void(*callback)(string, string))
{
	if (pInterface)
		pInterface->OnSearchListEvent_Callback = callback;
}
void CUIFuryInterfaceComponent::CallbackEvent_Set_DropDownList(void(*callback)(string, string))
{
	if (pInterface)
		pInterface->OnDropDownListEvent_Callback = callback;
}
void CUIFuryInterfaceComponent::CallbackEvent_Set_ButtonReleased(void(*callback)(string, string))
{
	if (pInterface)
		pInterface->OnButtonReleased_Callback = callback;
}
void CUIFuryInterfaceComponent::CallbackEvent_Set_ButtonPressed(void(*callback)(string, string))
{
	if (pInterface)
		pInterface->OnButtonPressed_Callback = callback;
}
void CUIFuryInterfaceComponent::CallbackEvent_Set_SwitchButton(void(*callback)(string, bool))
{
	if (pInterface)
		pInterface->OnSwitchButtonEvent_Callback = callback;
}
void CUIFuryInterfaceComponent::CallbackEvent_Set_InputText(void(*callback)(string, string))
{
	if (pInterface)
		pInterface->OnTextInputEvent_Callback = callback;
}
void CUIFuryInterfaceComponent::CallbackEvent_Set_ControlEnterLeave(void(*callback)(string, bool))
{
	if (pInterface)
		pInterface->OnControlEnterLeaveEvent_Callback = callback;
}
void CUIFuryInterfaceComponent::CallbackEvent_Set_DropItemEnterLeave(void(*callback)(int, bool))
{
	if (pInterface)
		pInterface->OnDropItemMouseEnterLeaveEvent_Callback = callback;
}
#pragma endregion SIGNALS
#pragma endregion SCHEMATYC

void CUIFuryInterfaceComponent::ComponentPropertyChanged()
{
	if (!gEnv->IsClient() || !pInterface)
		return;

	if (XmlNodeRef uiFuryNode = gEnv->pSystem->LoadXmlFromFile(settings.uiFuryFile.value.c_str()))
	{
		const string interfaceName = uiFuryNode->getTag();
		pInterface->InitializeInterface(interfaceName, show + interfaceName, hide + interfaceName);
	}

	LoadInterface();
}

void CUIFuryInterfaceComponent::LoadInterface()
{
	if (!gEnv->IsClient() || !pInterface)
		return;

	if (XmlNodeRef uiFuryNode = gEnv->pSystem->LoadXmlFromFile(settings.uiFuryFile.value.c_str()))
	{
		//We have to get interface node firstly
		if (XmlNodeRef interfaceNode = uiFuryNode->findChild("Interface"))
		{
			LoadInterfaceHierarchy(interfaceNode, "mainControl");
		}
		//Load logic
		if (XmlNodeRef logicNode = uiFuryNode->findChild("Logic"))
		{
			LoadInterfaceLogic(logicNode, nullptr);
		}
	}
	if (settings.bShowByDefault)
		Open();
	else
		Close();
}

void CUIFuryInterfaceComponent::LoadInterfaceHierarchy(XmlNodeRef parentXmlNode, string parentName)
{
	if (!parentXmlNode)
		return;

	const string PANEL = "Panel";
	const string LABEL = "Label";
	const string BUTTON = "Button";
	const string SWITCH_BUTTON = "SwitchButton";
	const string INPUT_TEXT = "Input_Text";
	const string DROP_DOWN_LIST = "DropDownList";
	const string SEARCH_LIST = "SearchList";
	const string DROP_TARGET = "DropTarget";
	const string MESSAGE_BOX = "MessageBox";

	const string _TRUE = "True";
	const string _FALSE = "False";

	for (int i = 0; i < parentXmlNode->getChildCount(); i++)
	{
		if (XmlNodeRef objectNode = parentXmlNode->getChild(i))
		{
			const string objectTypeName = objectNode->getTag();
			const bool bExportable = objectNode->getAttr("exportable") == _TRUE ? true : false;
			if (bExportable)
			{
				const bool bVisibleByDefault = objectNode->getAttr("visible_by_default") == _TRUE ? true : false;
				const string name = objectNode->getAttr("name");
				const int x_pos = atoi(objectNode->getAttr("x"));
				const int y_pos = atoi(objectNode->getAttr("y"));
				const int width = atoi(objectNode->getAttr("width"));
				const int height = atoi(objectNode->getAttr("height"));
				const string label_text = objectNode->getAttr("label_text");
				const int label_size = atoi(objectNode->getAttr("label_size"));
				const string label_font = objectNode->getAttr("label_font");
				string label_base_color = objectNode->getAttr("label_base_color");
				string label_hover_color = objectNode->getAttr("label_hover_color");
				string label_pressed_color = objectNode->getAttr("label_pressed_color");
				label_base_color.replace("#", "0x");
				label_hover_color.replace("#", "0x");
				label_pressed_color.replace("#", "0x");
				const string img_base = objectNode->getAttr("img_base");
				const string img_hover = objectNode->getAttr("img_hover");
				const string img_pressed = objectNode->getAttr("img_pressed");
				const string img_ico = objectNode->getAttr("img_ico");
				const string switch_group = objectNode->getAttr("swt_group");
				const int in_maxchars = atoi(objectNode->getAttr("in_maxchars"));
				const bool in_pass = objectNode->getAttr("in_pass") == _TRUE ? true : false;
				const string drop_class = objectNode->getAttr("drop_class");
				const int max_mes = atoi(objectNode->getAttr("max_mes"));

				pInterface->AddInterfaceObject(name, x_pos, y_pos, width, height, label_text, label_base_color, label_hover_color, label_pressed_color, label_font, label_size, img_base, img_hover, img_pressed, img_ico);

				if (objectTypeName == PANEL)
				{
					pInterface->AddPanel(parentName, name, x_pos, y_pos, width, height, img_base);
				}
				else if (objectTypeName == LABEL)
				{
					pInterface->AddLabel(parentName, name, x_pos, y_pos, label_size, label_base_color, label_font);
					pInterface->SetLabelText(name, label_text);
				}
				else if (objectTypeName == BUTTON)
				{
					if (!img_ico.empty())
						pInterface->AddIconButton(parentName, name, x_pos, y_pos, width, height, img_base, img_hover, img_pressed, img_ico);
					else
					{
						const bool bLabelOnly = img_base.empty();
						pInterface->AddButton(parentName, name, x_pos, y_pos, width, height, label_text, label_base_color, label_hover_color, label_pressed_color, label_font, label_size, bLabelOnly, img_base, img_hover, img_pressed);
					}
				}
				else if (objectTypeName == SWITCH_BUTTON)
				{
					const bool bLabelOnly = img_base.empty();
					pInterface->AddSwitchButton(parentName, name, switch_group, x_pos, y_pos, width, height, label_text, label_base_color, label_hover_color, label_pressed_color, label_font, label_size, bLabelOnly, img_base, img_hover, img_pressed, img_ico);
				}
				else if (objectTypeName == INPUT_TEXT)
				{
					pInterface->AddInputText(parentName, name, x_pos, y_pos, width, height, in_maxchars, label_size, label_font, label_base_color, label_text, in_pass);
				}
				else if (objectTypeName == DROP_DOWN_LIST)
				{
					pInterface->AddDropDownList(parentName, name, x_pos, y_pos, width, height, label_size, label_text, label_base_color, label_hover_color, label_pressed_color, label_font);
					//drop down has additional hierarchy
					if (XmlNodeRef dropDownHierarchy = objectNode->findChild("drop_down_item_list"))
					{
						for (int x = 0; x < dropDownHierarchy->getChildCount(); x++)
						{
							if (XmlNodeRef itemNode = dropDownHierarchy->getChild(x))
							{
								pInterface->AddItemToDropDownList(name, itemNode->getAttr("name"));
							}
						}
					}
				}
				else if (objectTypeName == SEARCH_LIST)
				{
					pInterface->AddSearchList(parentName, name, x_pos, y_pos, width, label_size, label_font, label_base_color, label_hover_color, img_ico);
					//search list has multi dimensional hierarchy, recursion required
					if (XmlNodeRef searchListHierarchy = objectNode->findChild("search_list_item_list"))
					{
						LoadSearchListItem(searchListHierarchy, name);
					}
				}
				else if (objectTypeName == DROP_TARGET)
				{
					pInterface->AddDropTarget(parentName, name, x_pos, y_pos, width, height, img_base, img_hover, img_pressed, drop_class);
				}
				else if (objectTypeName == MESSAGE_BOX)
				{
					pInterface->AddMessageBox(parentName, name, x_pos, y_pos, width, height, max_mes, img_base);
				}

				pInterface->ShowControl(name, bVisibleByDefault);
				LoadInterfaceHierarchy(objectNode, name);
			}
		}
	}
}

void CUIFuryInterfaceComponent::LoadSearchListItem(XmlNodeRef parentXmlNode, string listName)
{
	for (int x = 0; x < parentXmlNode->getChildCount(); x++)
	{
		if (XmlNodeRef itemNode = parentXmlNode->getChild(x))
		{
			const string sublistName = itemNode->getAttr("name");
			pInterface->AddItemToSearchList(listName, sublistName, itemNode->getChildCount() > 0);
			pInterface->AddSearchListEventCaller(sublistName);
			LoadSearchListItem(itemNode, sublistName + "_sublist");
		}
	}
}



void CUIFuryInterfaceComponent::LoadInterfaceLogic(XmlNodeRef parentXmlNode, IUIFuryNode *pParentNode)
{
	if (!parentXmlNode)
		return;

	for (int i = 0; i < parentXmlNode->getChildCount(); i++)
	{
		if (XmlNodeRef logicNode = parentXmlNode->getChild(i))
		{
			const string logicNodeName = logicNode->getTag();
			const string refObjName = logicNode->getAttr("name");
			const string ON_EVENT = "OnEvent_";
			const string SHOW_OBJECT = "Show_Object";
			const string HIDE_OBJECT = "Hide_Object";
			const string INPUT_TEXT_EMPTY = "IsInputTextEmpty";
			const string INPUT_TEXT_NOT_EMPTY = "IsInputTextNotEmpty";
			const string CRYENGINE_CONSOLE_VARIABLE = "Cryengine_Console_Variable";
			bool bIsEvent = true;
			//check if node is event
			if (logicNodeName.length() > ON_EVENT.length())
			{
				for (int i = 0; i < ON_EVENT.length(); i++)
				{
					if (logicNodeName[i] != ON_EVENT[i])
					{
						bIsEvent = false;
						break;
					}
				}
			}
			if (bIsEvent)
			{
				if (!pParentNode)
				{
					CEventNode *pEventNode = new CEventNode(logicNodeName, pParentNode, pInterface->GetInterfaceObject(refObjName), pInterface);
					pInterface->AddEventNode(pEventNode);
					LoadInterfaceLogic(logicNode, pEventNode);
				}
			}
			else if(pParentNode)
			{
				if (logicNodeName == SHOW_OBJECT)
				{
					CShowNode *pShowNode = new CShowNode(logicNodeName, pParentNode, true, pInterface->GetInterfaceObject(refObjName), pInterface);
				}
				else if (logicNodeName == HIDE_OBJECT)
				{
					CShowNode *pHideNode = new CShowNode(logicNodeName, pParentNode, false, pInterface->GetInterfaceObject(refObjName), pInterface);
				}
				else if (logicNodeName == INPUT_TEXT_EMPTY)
				{
					CIsInputTextEmpty *pIsEmptyNode = new CIsInputTextEmpty(logicNodeName, pParentNode, true, pInterface->GetInterfaceObject(refObjName), pInterface);
					LoadInterfaceLogic(logicNode, pIsEmptyNode);
				}
				else if (logicNodeName == INPUT_TEXT_NOT_EMPTY)
				{
					CIsInputTextEmpty *pIsEmptyNode = new CIsInputTextEmpty(logicNodeName, pParentNode, false, pInterface->GetInterfaceObject(refObjName), pInterface);
					LoadInterfaceLogic(logicNode, pIsEmptyNode);
				}
				else if (logicNodeName == CRYENGINE_CONSOLE_VARIABLE)
				{
					CCVarNode *pCvarNode = new CCVarNode(logicNodeName, pParentNode, pInterface, refObjName);
					LoadInterfaceLogic(logicNode, pCvarNode);
				}
			}
		}
	}
}


