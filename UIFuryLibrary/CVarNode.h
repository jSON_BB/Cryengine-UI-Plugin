#pragma once
#include "UIFuryNode.h"

class CCVarNode : public IUIFuryNode
{
public:
	CCVarNode(string sNodeName, IUIFuryNode *pNewParent, IUIFuryInterface *pNewInterface, string variable);

private:
	virtual void Run() override;
	const string sVariable;
};