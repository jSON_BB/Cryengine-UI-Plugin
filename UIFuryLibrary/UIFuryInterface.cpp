#include "StdAfx.h"
#include "UIFuryInterface.h"
#include "EventNode.h"
#include "IsInputTextEmpty.h"
#include "InterfaceObject.h"
#include "UIFuryInterfaceComponent.h"

IUIFuryInterface::IUIFuryInterface(CUIFuryInterfaceComponent *pcmpt)
{
	pComponent = pcmpt;
}

void IUIFuryInterface::OnUIEventEx(IUIElement* pSender, const char* fscommand, const SUIArguments& args, void* pUserData)
{
	const string eventName = fscommand;

	if (args.GetArgCount() > 0)
	{
		string mes;
		args.GetArg<string>(0, mes);

		std::size_t foundEnter = mes.find("control_enter");
		if (foundEnter != std::string::npos)
		{
			if (OnControlEnterLeaveEvent_Callback)
				OnControlEnterLeaveEvent_Callback(eventName, true);

			OnControlEnterLeaveEvent(eventName, true);
			_OnControlEnterLeaveEvent(eventName, true);
			return;
		}
		std::size_t foundLeave = mes.find("control_leave");
		if (foundLeave != std::string::npos)
		{
			if (OnControlEnterLeaveEvent_Callback)
				OnControlEnterLeaveEvent_Callback(eventName, false);

			OnControlEnterLeaveEvent(eventName, false);
			_OnControlEnterLeaveEvent(eventName, false);
			return;
		}
		std::size_t fountDropItemEnter = mes.find("drop_item_enter");
		if (fountDropItemEnter != std::string::npos)
		{
			string name = eventName;
			string id_str = eventName;
			id_str.erase(0, id_str.find_last_of("_") + 1);
			name.erase(name.find_last_of("_"));
			int id_int = atoi(id_str);

			if(OnDropItemMouseEnterLeaveEvent_Callback)
				OnDropItemMouseEnterLeaveEvent_Callback(id_int, true);
			return;
		}
		std::size_t fountDropItemLeave = mes.find("drop_item_leave");
		if (fountDropItemLeave != std::string::npos)
		{
			string name = eventName;
			string id_str = eventName;
			id_str.erase(0, id_str.find_last_of("_") + 1);
			name.erase(name.find_last_of("_"));
			int id_int = atoi(id_str);

			if (OnDropItemMouseEnterLeaveEvent_Callback)
				OnDropItemMouseEnterLeaveEvent_Callback(id_int, false);
			return;
		}
	}

	for (int i = 0; i < dropItemsEvents.size(); i++)
	{
		if (ToString(dropItemsEvents[i]) == eventName)
		{
			string dropTarget;
			args.GetArg<string>(0, dropTarget);
			OnDropItemEvents(dropItemsEvents[i], dropTarget);
			return;
		}
	}
	for (int i = 0; i < buttonEvents.size(); i++)
	{
		if (buttonEvents[i] == eventName)
		{
			const string prst = "pressed";
			const string rlsd = "released_outside";
			const string entr = "enter";
			const string rls = "released";
			const string lft = "leave";
			string message;
			args.GetArg<string>(0, message);
			if (message == prst)
			{
				if (OnButtonPressed_Callback)
				OnButtonPressed_Callback(eventName, message);

				_OnButtonPressed(eventName);
				OnButtonPressed(eventName);
			}
			else if (message == rls)
			{
				if (OnButtonReleased_Callback)
				OnButtonReleased_Callback(eventName, message);

				_OnButtonReleased(eventName);
				OnButtonReleased(eventName);
			}
			else if (message == rlsd)
			{
				OnButtonReleasedOutside(eventName);
			}
			else if (message == entr)
			{
				if (OnButtonEnter_Callback)
					OnButtonEnter_Callback(eventName, message);

				_OnButtonEnter(eventName);
				OnButtonEnter(eventName);
			}
			else if (message == lft)
			{
				if (OnButtonLeave_Callback)
				OnButtonLeave_Callback(eventName, message);

				_OnButtonLeave(eventName);
				OnButtonLeave(eventName);
			}

			return;
		}
	}
	for (int i = 0; i < switchButtonEvents.size(); i++)
	{
		if (switchButtonEvents[i] == eventName)
		{
			bool bSwitchOn = false;
			args.GetArg<bool>(0, bSwitchOn);
			if (OnSwitchButtonEvent_Callback)
				OnSwitchButtonEvent_Callback(eventName, bSwitchOn);

			_OnSwitchButtonEvent(eventName, bSwitchOn);
			OnSwitchButtonEvent(eventName, bSwitchOn);
			return;
		}
	}
	for (int i = 0; i < inputTextEvents.size(); i++)
	{
		if (inputTextEvents[i] == eventName)
		{
			string str;
			args.GetArg<string>(0, str);
			if (OnTextInputEvent_Callback)
			OnTextInputEvent_Callback(eventName, str);

			_OnTextInputEvent(eventName, str);
			OnTextInputEvent(eventName, str);
			return;
		}
	}
	for (int i = 0; i < dropDownListsEvents.size(); i++)
	{
		if (dropDownListsEvents[i] == eventName)
		{
			string str;
			args.GetArg<string>(0, str);
			if (OnDropDownListEvent_Callback)
			OnDropDownListEvent_Callback(eventName, str);

			_OnDropDownListEvent(eventName, str);
			OnDropDownListEvent(eventName, str);
			return;
		}
	}
	for (int i = 0; i < searchListEvents.size(); i++)
	{
		if (searchListEvents[i] == eventName)
		{
			string str;
			args.GetArg<string>(0, str);
			if (OnSearchListEvent_Callback)
			OnSearchListEvent_Callback(eventName, str);

			_OnSearchListEvent(eventName, str);
			OnSearchListEvent(eventName, str);
		}
	}
}

void IUIFuryInterface::AddScreen(string screenName)
{
	SUIArguments args;
	args.AddArgument<string>(screenName);

	pElement->CallFunction("AddScreen", args);
}

void IUIFuryInterface::AddInputText(string parentControl, string name, int x, int y, int w, int h, int maxChars, int size, string fontName, string fontColor, string startText, bool password)
{
	SUIArguments args;
	args.AddArgument<string>(parentControl);
	args.AddArgument<string>(name);
	args.AddArgument<int>(x);
	args.AddArgument<int>(y);
	args.AddArgument<int>(w);
	args.AddArgument<int>(h);
	args.AddArgument<int>(maxChars);
	args.AddArgument<int>(size);
	args.AddArgument<string>(fontName);
	args.AddArgument<string>(fontColor);
	args.AddArgument<string>(startText);
	args.AddArgument<bool>(password);

	pElement->CallFunction("AddInputText", args);

	inputTextEvents.push_back(name);
}

void IUIFuryInterface::AddLabel(string parentControl, string name, int x, int y, int size, string fontColor, string fontName)
{
	SUIArguments args;
	args.AddArgument<string>(parentControl);
	args.AddArgument<string>(name);
	args.AddArgument<int>(x);
	args.AddArgument<int>(y);
	args.AddArgument<int>(size);
	args.AddArgument<string>(fontColor);
	args.AddArgument<string>(fontName);

	pElement->CallFunction("AddLabel", args);
}

void IUIFuryInterface::AddPanel(string parentControl, string name, int x, int y, int w, int h, string bgImage, string additionalIcon)
{
	SUIArguments args;
	args.AddArgument<string>(parentControl);
	args.AddArgument<string>(name);
	args.AddArgument<int>(x);
	args.AddArgument<int>(y);
	args.AddArgument<int>(w);
	args.AddArgument<int>(h);
	args.AddArgument<string>(bgImage);
	args.AddArgument<string>(additionalIcon);

	pElement->CallFunction("AddPanel", args);
}

void IUIFuryInterface::AddButton(string parentControl, string name, int x, int y, int w, int h, string label, string labelBaseColor, string labelHoverColor, string labelPressColor, string labelFont, int fontSize, bool labelOnly, string baseImage, string hoverImage, string pressImage)
{
	SUIArguments args;
	args.AddArgument<string>(parentControl);
	args.AddArgument<string>(name);
	args.AddArgument<int>(x);
	args.AddArgument<int>(y);
	args.AddArgument<int>(w);
	args.AddArgument<int>(h);
	args.AddArgument<string>(label);
	args.AddArgument<string>(labelBaseColor);
	args.AddArgument<string>(labelHoverColor);
	args.AddArgument<string>(labelPressColor);
	args.AddArgument<string>(labelFont);
	args.AddArgument<int>(fontSize);
	args.AddArgument<bool>(labelOnly);
	args.AddArgument<string>(baseImage);
	args.AddArgument<string>(hoverImage);
	args.AddArgument<string>(pressImage);

	pElement->CallFunction("AddButton", args);

	buttonEvents.push_back(name);
}

void IUIFuryInterface::ShowControl(string controlName, bool show)
{
	SUIArguments args;
	args.AddArgument<bool>(show);
	args.AddArgument<string>(controlName);

	pElement->CallFunction("ShowControl", args);
}

void IUIFuryInterface::AlignToParent(string controlName, int alignType, int translate_x, int translate_y)
{
	SUIArguments args;
	args.AddArgument<string>(controlName);
	args.AddArgument<int>(alignType);
	args.AddArgument<int>(translate_x);
	args.AddArgument<int>(translate_y);

	pElement->CallFunction("AlignToParent", args);
}

void IUIFuryInterface::Translate(string controlName, int x, int y)
{
	SUIArguments args;
	args.AddArgument<string>(controlName);
	args.AddArgument<int>(x);
	args.AddArgument<int>(y);

	pElement->CallFunction("Translate", args);
}

void IUIFuryInterface::SetLabelText(string controlName, string newText)
{
	SUIArguments args;
	args.AddArgument<string>(controlName);
	args.AddArgument<string>(newText);

	pElement->CallFunction("SetLabelText", args);
}

void IUIFuryInterface::AddDropDownList(string parentControl, string name, int x, int y, int w, int h, int fontSize, string label, string labelBaseColor, string labelHoverColor, string labelPressColor, string labelFont)
{
	SUIArguments args;
	args.AddArgument<string>(parentControl);
	args.AddArgument<string>(name);
	args.AddArgument<int>(x);
	args.AddArgument<int>(y);
	args.AddArgument<int>(w);
	args.AddArgument<int>(h);
	args.AddArgument<int>(fontSize);
	args.AddArgument<string>(label);
	args.AddArgument<string>(labelFont);
	args.AddArgument<string>(labelBaseColor);
	args.AddArgument<string>(labelHoverColor);
	args.AddArgument<string>(labelPressColor);

	pElement->CallFunction("AddDropDownList", args);

	dropDownListsEvents.push_back(name);
}

void IUIFuryInterface::AddItemToDropDownList(string parentList, string text)
{
	SUIArguments args;
	args.AddArgument<string>(parentList);
	args.AddArgument<string>(text);

	pElement->CallFunction("AddItemToDropDownList", args);
}

void IUIFuryInterface::AddSearchList(string parentControl, string name, int x, int y, int w, int fontSize, string fontName, string fontColorBase, string fontColorHover, string expIcon)
{
	SUIArguments args;
	args.AddArgument<string>(parentControl);
	args.AddArgument<string>(name);
	args.AddArgument<int>(x);
	args.AddArgument<int>(y);
	args.AddArgument<int>(w);
	args.AddArgument<int>(fontSize);
	args.AddArgument<string>(fontName);
	args.AddArgument<string>(fontColorBase);
	args.AddArgument<string>(fontColorHover);
	args.AddArgument<string>(expIcon);

	pElement->CallFunction("AddSearchList", args);

	searchListEvents.push_back(name);
}

void IUIFuryInterface::AddItemToSearchList(string parentList, string text, bool expandable)
{
	SUIArguments args;
	args.AddArgument<string>(parentList);
	args.AddArgument<string>(text);
	args.AddArgument<bool>(expandable);

	pElement->CallFunction("AddItemToSearchList", args);
}

void IUIFuryInterface::AddIconButton(string parentControl, string name, int x, int y, int w, int h, string baseBg, string hoverBg, string pressBg, string icon)
{
	SUIArguments args;
	args.AddArgument<string>(parentControl);
	args.AddArgument<string>(name);
	args.AddArgument<int>(x);
	args.AddArgument<int>(y);
	args.AddArgument<int>(w);
	args.AddArgument<int>(h);
	args.AddArgument<string>(baseBg);
	args.AddArgument<string>(hoverBg);
	args.AddArgument<string>(pressBg);
	args.AddArgument<string>(icon);

	pElement->CallFunction("AddIconButton", args);

	buttonEvents.push_back(name);
}

void IUIFuryInterface::ResetDropDownList(string listName, bool bClear)
{
	SUIArguments args;
	args.AddArgument<string>(listName);
	args.AddArgument<bool>(bClear);

	pElement->CallFunction("ResetDropDownList", args);
}

void IUIFuryInterface::AddSwitchButton(string parentControl, string name, string groupName, int x, int y, int w, int h, string label, string labelBaseColor, string labelHoverColor, string labelPressColor, string labelFont, int fontSize, bool labelOnly, string baseImage, string hoverImage, string pressImage, string additionalIcon)
{
	SUIArguments args;
	args.AddArgument<string>(parentControl);
	args.AddArgument<string>(name);
	args.AddArgument<string>(groupName);
	args.AddArgument<int>(x);
	args.AddArgument<int>(y);
	args.AddArgument<int>(w);
	args.AddArgument<int>(h);
	args.AddArgument<string>(label);
	args.AddArgument<string>(labelBaseColor);
	args.AddArgument<string>(labelHoverColor);
	args.AddArgument<string>(labelPressColor);
	args.AddArgument<string>(labelFont);
	args.AddArgument<int>(fontSize);
	args.AddArgument<bool>(labelOnly);
	args.AddArgument<string>(baseImage);
	args.AddArgument<string>(hoverImage);
	args.AddArgument<string>(pressImage);
	args.AddArgument<string>(additionalIcon);

	pElement->CallFunction("AddSwitchButton", args);

	switchButtonEvents.push_back(name);
}

void IUIFuryInterface::AddDropTarget(string parentControl, string name, int x, int y, int w, int h, string imgBase, string imgAvailable, string imgNotAvailable, string dropClass)
{
	SUIArguments args;
	args.AddArgument<string>(parentControl);
	args.AddArgument<string>(name);
	args.AddArgument<int>(x);
	args.AddArgument<int>(y);
	args.AddArgument<int>(w);
	args.AddArgument<int>(h);
	args.AddArgument<string>(imgBase);
	args.AddArgument<string>(imgAvailable);
	args.AddArgument<string>(imgNotAvailable);
	args.AddArgument<string>(dropClass);

	pElement->CallFunction("AddDropTarget", args);
}

void IUIFuryInterface::AddDropItemToTarget(string name, int uniqueId, string targetName, string dropClass, string dropClass_second, int w, int h, string img, bool snap)
{
	SUIArguments args;
	args.AddArgument<string>(name);
	args.AddArgument<int>(uniqueId);
	args.AddArgument<string>(targetName);
	args.AddArgument<string>(dropClass);
	args.AddArgument<string>(dropClass_second);
	args.AddArgument<int>(w);
	args.AddArgument<int>(h);
	args.AddArgument<string>(img);
	args.AddArgument<bool>(snap);

	pElement->CallFunction("AddDropItemToTarget", args);

	dropItemsEvents.push_back(uniqueId);
}

void IUIFuryInterface::ClearDropTarget(string targetName)
{
	SUIArguments args;
	args.AddArgument<string>(targetName);

	pElement->CallFunction("ClearDropTarget", args);
}

void IUIFuryInterface::RemoveControl(string controlName)
{
	if (controlName.empty())
		return;

	SUIArguments args;
	args.AddArgument<string>(controlName);

	pElement->CallFunction("RemoveControl", args);
}

void IUIFuryInterface::SetControlPosition(string controlName, int x, int y)
{
	if (controlName.empty())
		return;

	SUIArguments args;
	args.AddArgument<string>(controlName);
	args.AddArgument<int>(x);
	args.AddArgument<int>(y);

	pElement->CallFunction("SetControlPosition", args);
}

void IUIFuryInterface::SetButtonLabel(string name, string text)
{
	if (name.empty())
		return;

	SUIArguments args;
	args.AddArgument<string>(name);
	args.AddArgument<string>(text);

	pElement->CallFunction("SetButtonLabel", args);
}

void IUIFuryInterface::SetPanelIcon(string controlName, string icon)
{
	if (controlName.empty())
		return;

	SUIArguments args;
	args.AddArgument<string>(controlName);
	args.AddArgument<string>(icon);

	pElement->CallFunction("SetPanelIcon", args);
}

void IUIFuryInterface::CreateMask(string name, string controlName, string image, int alpha)
{
	if (name.empty())
		return;

	SUIArguments args;
	args.AddArgument<string>(name);
	args.AddArgument<string>(controlName);
	args.AddArgument<string>(image);
	args.AddArgument<int>(alpha);

	pElement->CallFunction("CreateMask", args);
}

void IUIFuryInterface::KillMask(string name)
{
	if (name.empty())
		return;

	SUIArguments args;
	args.AddArgument<string>(name);

	pElement->CallFunction("KillMask", args);
}

void IUIFuryInterface::AddMessageBox(string parentControl, string name, int x, int y, int w, int h, int maxMes, string image)
{
	if (parentControl.empty() || name.empty())
		return;

	SUIArguments args;
	args.AddArgument<string>(parentControl);
	args.AddArgument<string>(name);
	args.AddArgument<int>(x);
	args.AddArgument<int>(y);
	args.AddArgument<int>(w);
	args.AddArgument<int>(h);
	args.AddArgument<int>(maxMes);
	args.AddArgument<string>(parentControl);

	pElement->CallFunction("AddMessageBox", args);
}

void IUIFuryInterface::AddMessageToMessageBox(string boxName, string message, string color, string fontName, int size)
{
	if (boxName.empty() || message.empty())
		return;

	SUIArguments args;
	args.AddArgument<string>(boxName);
	args.AddArgument<string>(message);
	args.AddArgument<string>(color);
	args.AddArgument<string>(fontName);
	args.AddArgument<int>(size);

	pElement->CallFunction("AddMessageToMessageBox", args);
}

void IUIFuryInterface::EmptyInputText(string controlName)
{
	SUIArguments args;
	args.AddArgument<string>(controlName);

	pElement->CallFunction("EmptyInputText", args);
}

void IUIFuryInterface::ShowElement()
{
	if (!pShow || !pElement || !pManager)
		return;

	pManager->StartAction(pShow, pElement->GetName());
}

void IUIFuryInterface::HideElement()
{
	if (!pShow || !pElement || !pManager)
		return;

	pManager->StartAction(pHide, pElement->GetName());
}

void IUIFuryInterface::ToggleMenus(string menuToOpen)
{
	ShowControl(sCurrentMenu, false);
	sCurrentMenu = menuToOpen;
	ShowControl(sCurrentMenu, true);
}

void IUIFuryInterface::SetElement(string sElementName)
{
	pElement = gEnv->pFlashUI->GetUIElement(sElementName);
}

void IUIFuryInterface::SetShowAction(string sActionName)
{
	pShow = gEnv->pFlashUI->GetUIAction(sActionName);
}

void IUIFuryInterface::SetHideAction(string sActionName)
{
	pHide = gEnv->pFlashUI->GetUIAction(sActionName);
}

void IUIFuryInterface::StartAction(string sActionName, bool bStart)
{
	if (!pElement || !pManager)
		return;

	IUIAction *pCustomAction = gEnv->pFlashUI->GetUIAction(sActionName);
	
	if (!pCustomAction)
		return;

	if (bStart)
		pManager->StartAction(pCustomAction, pElement->GetName());
	else
		pManager->EndAction(pCustomAction, pElement->GetName());
}

void IUIFuryInterface::InitializeInterface(string elementName, string showActionName, string hideActionName)
{
	pFlash = gEnv->pFlashUI;

	if (!pFlash)
		return;

	pElement = pFlash->GetUIElement(elementName);
	pShow = pFlash->GetUIAction(showActionName);
	pHide = pFlash->GetUIAction(hideActionName);
	pManager = pFlash->GetUIActionManager();

	if (!pElement || !pShow || !pHide || !pManager)
	{
		delete this;
		return;
	}

	pElement->AddEventListener(this, elementName);
}

void IUIFuryInterface::AddEventNode(CEventNode *pEventNode)
{
	logicEvents.push_back(pEventNode);
}

CEventNode * IUIFuryInterface::GetEventNode(string eventNodeName, string refObjName)
{
	for (int i = 0; i < logicEvents.size(); i++)
	{
		if (CEventNode *foundNode = logicEvents[i])
		{
			if (foundNode->GetNodeName() == eventNodeName)
			{
				if (foundNode->GetRefObjName() == refObjName)
				{
					return foundNode;
				}
			}
		}
	}
	return nullptr;
}

void IUIFuryInterface::AddInterfaceObject(string objName, int x, int y, int w, int h, string labelText, string labelBaseColor, string labelHoverColor, string labelPressColor, string labelFont, int fontSize, string baseImage, string hoverImage, string pressImage, string additionalImage, string dropClass)
{
	SInterfaceObject *pNewObj = new SInterfaceObject(interfaceObjects.size(), objName, x, y, w, h, labelText, labelBaseColor, labelHoverColor, labelPressColor, labelFont, fontSize, baseImage, hoverImage, pressImage, additionalImage, dropClass);
	interfaceObjects.push_back(pNewObj);
}

SInterfaceObject * IUIFuryInterface::GetInterfaceObject(string objName)
{
	if (objName.empty())
		return nullptr;

	for each (SInterfaceObject *pObj in interfaceObjects)
	{
		if (pObj->sName == objName)
		{
			return pObj;
		}
	}

	return nullptr;
}

void IUIFuryInterface::AddSearchListEventCaller(string listName)
{
	searchListEvents.push_back(listName);
}

void IUIFuryInterface::_OnButtonPressed(string buttonName)
{
	pComponent->Schematyc_OnButtonPressedEvent(buttonName);

	if (CEventNode *pEventNode = GetEventNode("OnEvent_ButtonPressed", buttonName))
		pEventNode->RunNode();
}

void IUIFuryInterface::_OnButtonReleased(string buttonName)
{
	pComponent->Schematyc_OnButtonReleasedEvent(buttonName);

	if (CEventNode *pEventNode = GetEventNode("OnEvent_ButtonReleased", buttonName))
		pEventNode->RunNode();
}

void IUIFuryInterface::_OnButtonEnter(string buttonName)
{
	pComponent->Schematyc_OnObjectMouseEnterEvent(buttonName);

	if (CEventNode *pEventNode = GetEventNode("OnEvent_ObjectMouseEnter", buttonName))
		pEventNode->RunNode();
}

void IUIFuryInterface::_OnButtonLeave(string buttonName)
{
	pComponent->Schematyc_OnObjectMouseLeaveEvent(buttonName);

	if (CEventNode *pEventNode = GetEventNode("OnEvent_ObjectMouseLeave", buttonName))
		pEventNode->RunNode();
}

void IUIFuryInterface::_OnSwitchButtonEvent(string buttonName, bool bOn)
{
	pComponent->Schematyc_OnSwitchButtonEvent(buttonName);

	if (CEventNode *pEventNode = GetEventNode("OnEvent_SwitchButtonEvent", buttonName))
		pEventNode->RunNode();
}

void IUIFuryInterface::_OnTextInputEvent(string inputName, string currentText)
{
	pComponent->Schematyc_OnInputTextValueChangedEvent(inputName, currentText);

	if (SInterfaceObject *pInterfaceObject = GetInterfaceObject(inputName))
		pInterfaceObject->sLabelText = currentText;
	if (CEventNode *pEventNode = GetEventNode("OnEvent_InputTextChanged", inputName))
		pEventNode->RunNode();
}

void IUIFuryInterface::_OnDropDownListEvent(string listName, string currentValue)
{
	pComponent->Schematyc_OnDropDownListValueChangedEvent(listName, currentValue);

	if (SInterfaceObject *pInterfaceObject = GetInterfaceObject(listName))
		pInterfaceObject->sLabelText = currentValue;
	if (CEventNode *pEventNode = GetEventNode("OnEvent_DropDownListValueChanged", listName))
		pEventNode->RunNode();
}

void IUIFuryInterface::_OnSearchListEvent(string listName, string currentValue)
{
	pComponent->Schematyc_OnSearchListValueChangedEvent(listName, currentValue);

	if (CEventNode *pEventNode = GetEventNode("OnEvent_SearchListValueSelected", listName))
		pEventNode->RunNode();
}

void IUIFuryInterface::_OnControlEnterLeaveEvent(string controlName, bool bEnter)
{

}
