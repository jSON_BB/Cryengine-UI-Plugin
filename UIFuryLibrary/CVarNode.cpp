#include "StdAfx.h"
#include "CVarNode.h"

CCVarNode::CCVarNode(string sNodeName, IUIFuryNode * pNewParent, IUIFuryInterface * pNewInterface, string variable)
	: IUIFuryNode(sNodeName, pNewParent, nullptr, pNewInterface)
	, sVariable(variable)
{
}

void CCVarNode::Run()
{
	IConsole *pConsole = gEnv->pConsole;

	if (!pConsole || sVariable.empty())
		return;

	pConsole->ExecuteString(sVariable, false, true);
}
