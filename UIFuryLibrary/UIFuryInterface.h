#pragma once

#include <FlashUI/FlashUI.h>

class CEventNode;
struct SInterfaceObject;
class CUIFuryInterfaceComponent;

enum AlignType
{
	Top_Left = 0,
	Top_Mid,
	Top_Right,
	Mid_Left,
	Center,
	Mid_Right,
	Bot_Left,
	Bot_Mid,
	Bot_Right
};

struct IUIFuryInterface : public IUIElementEventListener
{
	IUIFuryInterface::IUIFuryInterface(CUIFuryInterfaceComponent *pcmpt);
	IUIFuryInterface::~IUIFuryInterface() {}

	virtual void OnUIEventEx(IUIElement* pSender, const char* fscommand, const SUIArguments& args, void* pUserData) override;

	virtual void AddScreen(string screenName);
	virtual void AddInputText(string parentControl, string name, int x, int y, int w, int h, int maxChars, int size, string fontName, string fontColor, string startText, bool password);
	virtual void AddLabel(string parentControl, string name, int x, int y, int size, string fontColor, string fontName);
	virtual void AddPanel(string parentControl, string name, int x, int y, int w, int h, string bgImage = "", string additionalIcon = "");
	virtual void AddButton(string parentControl, string name, int x, int y, int w, int h, string label, string labelBaseColor, string labelHoverColor, string labelPressColor, string labelFont, int fontSize, bool labelOnly = true, string baseImage = "", string hoverImage = "", string pressImage = "");
	virtual void ShowControl(string controlName, bool show);
	virtual void AlignToParent(string controlName, int alignType, int translate_x = 0, int translate_y = 0);
	virtual void Translate(string controlName, int x, int y);
	virtual void SetLabelText(string controlName, string newText);
	virtual void AddDropDownList(string parentControl, string name, int x, int y, int w, int h, int fontSize, string label, string labelBaseColor, string labelHoverColor, string labelPressColor, string labelFont);
	virtual void AddItemToDropDownList(string parentList, string text);
	virtual void AddSearchList(string parentControl, string name, int x, int y, int w, int fontSize, string fontName, string fontColorBase, string fontColorHover, string expIcon);
	virtual void AddItemToSearchList(string parentList, string text, bool expandable);
	virtual void AddIconButton(string parentControl, string name, int x, int y, int w, int h, string baseBg, string hoverBg, string pressBg, string icon);
	virtual void ResetDropDownList(string listName, bool bClear = false);
	virtual void AddSwitchButton(string parentControl, string name, string groupName, int x, int y, int w, int h, string label, string labelBaseColor, string labelHoverColor, string labelPressColor, string labelFont, int fontSize, bool labelOnly = true, string baseImage = "", string hoverImage = "", string pressImage = "", string additionalIcon = "");
	virtual void AddDropTarget(string parentControl, string name, int x, int y, int w, int h, string imgBase, string imgAvailable, string imgNotAvailable, string dropClass);
	virtual void AddDropItemToTarget(string name, int uniqueId, string targetName, string dropClass, string dropClass_second, int w, int h, string img, bool snap);
	virtual void ClearDropTarget(string targetName);
	virtual void RemoveControl(string controlName);
	virtual void SetControlPosition(string controlName, int x, int y);
	virtual void SetButtonLabel(string name, string text);
	virtual void SetPanelIcon(string controlName, string icon);
	virtual void CreateMask(string name, string controlName, string image, int alpha);
	virtual void KillMask(string name);
	virtual void AddMessageBox(string parentControl, string name, int x, int y, int w, int h, int maxMes, string image);
	virtual void AddMessageToMessageBox(string boxName, string message, string color, string fontName, int size);
	virtual void EmptyInputText(string controlName);

	virtual void ShowElement();
	virtual void HideElement();

private:
	virtual void ToggleMenus(string menuToOpen);
public:

	virtual void SetElement(string sElementName);
	virtual void SetShowAction(string sActionName);
	virtual void SetHideAction(string sActionName);

	virtual void StartAction(string sActionName, bool bStart);

	virtual void InitializeInterface(string elementName, string showActionName, string hideActionName);

	virtual void AddEventNode(CEventNode *pEventNode);

	virtual void AddSearchListEventCaller(string listName);

	virtual void AddInterfaceObject(string objName, int x = 0, int y = 0, int w = 0, int h = 0, string label = "", string labelBaseColor = "", string labelHoverColor = "", string labelPressColor = "", string labelFont = "", int fontSize = 0, string baseImage = "", string hoverImage = "", string pressImage = "", string additionalImage = "", string dropClass = "");
	virtual SInterfaceObject *GetInterfaceObject(string objName);

protected:
	virtual void OnButtonPressed(string buttonName) {}
	virtual void OnButtonReleased(string buttonName) {}
	virtual void OnButtonReleasedOutside(string buttonName) {}
	virtual void OnButtonEnter(string buttonName) {}
	virtual void OnButtonLeave(string buttonName) {}
	virtual void OnSwitchButtonEvent(string buttonName, bool bOn) {}
	virtual void OnTextInputEvent(string inputName, string currentText) {}
	virtual void OnDropDownListEvent(string listName, string currentValue) {}
	virtual void OnSearchListEvent(string listName, string currentValue) {}
	virtual void OnDropItemEvents(int uniqueId, string dropTarget) {}
	virtual void OnControlEnterLeaveEvent(string controlName, bool bEnter) {}
	virtual void OnDropItemMouseEnterLeaveEvent(int uniqueId, bool bEnter) {}

	virtual CEventNode *GetEventNode(string eventNodeName, string refObjName);
	//hidden event callbacks
private:
	virtual void _OnButtonPressed(string buttonName);
	virtual void _OnButtonReleased(string buttonName);
	virtual void _OnButtonEnter(string buttonName);
	virtual void _OnButtonLeave(string buttonName);
	virtual void _OnSwitchButtonEvent(string buttonName, bool bOn);
	virtual void _OnTextInputEvent(string inputName, string currentText);
	virtual void _OnDropDownListEvent(string listName, string currentValue);
	virtual void _OnSearchListEvent(string listName, string currentValue);
	virtual void _OnControlEnterLeaveEvent(string controlName, bool bEnter);
	virtual void _OnDropItemMouseEnterLeaveEvent(int uniqueId, bool bEnter) {}

public:
	//callbacks
	void (*OnButtonPressed_Callback)(string, string) = nullptr;
	void (*OnButtonReleased_Callback)(string, string) = nullptr;
	void (*OnButtonEnter_Callback)(string, string) = nullptr;
	void (*OnButtonLeave_Callback)(string, string) = nullptr;
	void (*OnTextInputEvent_Callback)(string, string) = nullptr;
	void (*OnDropDownListEvent_Callback)(string, string) = nullptr;
	void (*OnSearchListEvent_Callback)(string, string) = nullptr;
	void (*OnSwitchButtonEvent_Callback)(string, bool) = nullptr;
	void (*OnControlEnterLeaveEvent_Callback)(string, bool) = nullptr;
	void (*OnDropItemMouseEnterLeaveEvent_Callback)(int, bool) = nullptr;

protected:
	IUIElement *pElement = nullptr;
	IUIAction *pShow = nullptr;
	IUIAction *pHide = nullptr;
	IUIActionManager *pManager = nullptr;
	IFlashUI *pFlash = nullptr;

	std::vector<string> switchButtonEvents;
	std::vector<string> buttonEvents;
	std::vector<string> inputTextEvents;
	std::vector<string> dropDownListsEvents;
	std::vector<string> searchListEvents;
	std::vector<int> dropItemsEvents;

	std::vector<CEventNode*> logicEvents;

	std::vector<SInterfaceObject*> interfaceObjects;

	string sCurrentMenu;

	const string MAIN_CONTROL = "mainControl";

	CUIFuryInterfaceComponent *pComponent = nullptr;
};