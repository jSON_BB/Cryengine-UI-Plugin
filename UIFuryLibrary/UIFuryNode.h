#pragma once

struct IUIFuryInterface;
struct SInterfaceObject;

struct IUIFuryNode
{
	IUIFuryNode(string sNodeName, IUIFuryNode *pNewParent, SInterfaceObject *pRefInterfaceObj, IUIFuryInterface *pNewInterface);

	virtual void AddChild(IUIFuryNode *pChildNode);
	virtual void RunNode();
	string GetNodeName() { return sName; }
	string GetRefObjName();

protected:
	virtual void Run() = 0;

protected:
	const string sName;
	std::vector<IUIFuryNode*> children;
	IUIFuryNode *pParent = nullptr;
	SInterfaceObject *pRefObj = nullptr;
	IUIFuryInterface *pInterface = nullptr;
};