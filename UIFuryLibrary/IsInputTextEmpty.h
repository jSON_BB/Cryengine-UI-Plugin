#pragma once
#include "UIFuryNode.h"

class CIsInputTextEmpty : public IUIFuryNode
{
public:
	CIsInputTextEmpty(string sNodeName, IUIFuryNode *pNewParent, bool empty, SInterfaceObject *pRefInterfaceObj, IUIFuryInterface *pNewInterface);

	virtual void RunNode() override;
private:
	virtual void Run() {};

private:
	const bool bEmpty;
};